---
title: DrBearhands' writings

---

## Blog posts
* [The trias politica needs an IT branch](tech-and-society/trias-politica-it)

## Tutorials
* [Haskell for madmen](haskell-tutorial)
