PANDOC=pandoc
SRC_DIR=src
PFLAGS=-t html5 -r markdown --mathml -s --template=$(SRC_DIR)/blog_template -A "$(SRC_DIR)//style/pandoc.html"
OUT_DIR=public
UPLOAD=upload
SRC_MD_FILES=$(filter-out $(SRC_DIR)/index.md, $(shell find $(SRC_DIR)/ -type f -name "*.md") )
OUT_MD_FILES=$(patsubst $(SRC_DIR)/%.md,$(OUT_DIR)/%/index.html,$(SRC_MD_FILES))
UPLOAD_FILES=$(patsubst $(SRC_DIR)/%.md,$(UPLOAD)/%,$(SRC_MD_FILES))
SRC_CSS=$(shell find $(SRC_DIR)/ -type f -name '*.css')
OUT_CSS=$(patsubst $(SRC_DIR)/%.css,$(OUT_DIR)/%.css,$(SRC_CSS))
SRC_PNG=$(shell find $(SRC_DIR)/ -type f -name '*.png')
OUT_PNG=$(patsubst $(SRC_DIR)/%.png,$(OUT_DIR)/%.png,$(SRC_PNG))
SRC_JPG=$(shell find $(SRC_DIR)/ -type f -name '*.jpg')
OUT_JPG=$(patsubst $(SRC_DIR)/%.jpg,$(OUT_DIR)/%.jpg,$(SRC_JPG))


.phony: all clean brave_secret

all: $(OUT_MD_FILES) $(OUT_CSS) $(OUT_PNG) $(OUT_JPG) $(OUT_DIR)/index.html $(OUT_DIR)/.well-known/brave-rewards-verification.txt

clean:
	rm -rf $(OUT_DIR)

$(OUT_DIR)/.well-known/:
	mkdir $(OUT_DIR)/.well-known

$(OUT_DIR)/.well-known/brave-rewards-verification.txt: $(OUT_DIR)/.well-known/
	cp .well-known/brave-rewards-verification.txt $@

HASKELL_POSTS=$(shell curl https://dev.to/api/articles?username=drbearhands \
	| grep -Po '"canonical_url": *\K"[^"]*"' \
	| grep "haskell" \
	| grep -oP 'https://dev.to/drbearhands/\K[^"]*' )


$(OUT_DIR)/index.html: $(SRC_DIR)/index.md $(SRC_DIR)/blog_template.html5 $(SRC_DIR)/style/pandoc.html
	mkdir -p $(shell dirname $@)
	$(PANDOC) $(PFLAGS) -o $@ $<

$(OUT_DIR)/%/index.html: $(SRC_DIR)/%.md $(SRC_DIR)/blog_template.html5 $(SRC_DIR)/style/pandoc.html
	mkdir -p $(shell dirname $@)
	$(PANDOC) $(PFLAGS) -o $@ $<

$(OUT_DIR)/%.css: $(SRC_DIR)/%.css
	mkdir -p $(shell dirname $@)
	cp $< $@

$(OUT_DIR)/%.png: $(SRC_DIR)/%.png
	mkdir -p $(shell dirname $@)
	cp $< $@

$(OUT_DIR)/%.jpg: $(SRC_DIR)/%.jpg
	mkdir -p $(shell dirname $@)
	cp $< $@
